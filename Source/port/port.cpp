// Copyright Epic Games, Inc. All Rights Reserved.

#include "port.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, port, "port" );

DEFINE_LOG_CATEGORY(Logport)
 