// Copyright Epic Games, Inc. All Rights Reserved.

#include "portGameMode.h"
#include "portPlayerController.h"
#include "portCharacter.h"
#include "UObject/ConstructorHelpers.h"

AportGameMode::AportGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AportPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/myfolder/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}